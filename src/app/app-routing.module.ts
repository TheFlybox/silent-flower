import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from './core/enums/app-routes.enum';

const routes: Routes = [
  {
    path: AppRoutes.CUSTOMERS,
    loadChildren: () =>
      import('./features/customers/customers.module').then(
        (m) => m.CustomersModule
      ),
  },
  {
    path: '**',
    redirectTo: AppRoutes.CUSTOMERS
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

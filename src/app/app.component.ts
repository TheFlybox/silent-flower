import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { skip } from 'rxjs';
import { StorageKeys } from './core/enums/storage-keys.enum';
import { LocalStorageService } from './core/services/local-storage.service';
import { AppState } from './store/app.state';
import { customers } from './store/customers/customers.selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  constructor(
    private store: Store<AppState>,
    private localStorageService: LocalStorageService,
  ) {}

  ngOnInit(): void {
    this.store.select(customers).pipe(skip(1)).subscribe(data => {
      const customerList = data.records ?? [];
      this.localStorageService.removeFromStorage(StorageKeys.CUSTOMERS);
      this.localStorageService.addToStorage(StorageKeys.CUSTOMERS, JSON.stringify(customerList));
    });
  }
}

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppRoutes, CustomersRoutes } from 'src/app/core/enums/app-routes.enum';
import { CustomerStatus } from 'src/app/core/enums/customers-status.enum';
import { Customer } from 'src/app/core/models/customer.model';

@Injectable()
export class CustomersHelper {
  constructor(private router: Router) {}
  
  convertFormDataToCustomer(customerId: string, data: Partial<Omit<Customer, 'id'>>): Customer {
    return {
      id: customerId,
      firstName: data.firstName ?? '',
      lastName: data.lastName ?? '',
      status: data.status ?? CustomerStatus.ACTIVE,
      email: data.email ?? '',
      phone: data.phone ?? '',
      identity: data.identity
    }
  }
  public goToCustomersList() {
    this.router.navigate(
      [`../${AppRoutes.CUSTOMERS}`, `${CustomersRoutes.LIST}`]
    );
  }
}

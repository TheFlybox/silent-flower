import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomersRoutes } from 'src/app/core/enums/app-routes.enum';
import { CustomersCreateEditComponent } from './components/customers-create-edit/customers-create-edit.component';
import { CustomersDetailComponent } from './components/customers-detail/customers-detail.component';
import { CustomersListComponent } from './components/customers-list/customers-list.component';
import { CustomerResolver } from './resolvers/customer.resolver';

const routes: Routes = [
  { path: '', redirectTo: CustomersRoutes.LIST, pathMatch: 'full' },
  { path: CustomersRoutes.LIST, component: CustomersListComponent },
  { path: CustomersRoutes.CREATE, component: CustomersCreateEditComponent },
  {
    path: CustomersRoutes.EDIT,
    component: CustomersCreateEditComponent,
    resolve: { customer: CustomerResolver },
  },
  {
    path: CustomersRoutes.DETAIL,
    component: CustomersDetailComponent,
    resolve: { customer: CustomerResolver },
  },
  { path: '**', redirectTo: CustomersRoutes.LIST },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule { }

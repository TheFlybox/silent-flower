import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { CustomerService } from 'src/app/core/services/customer.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { AppState } from 'src/app/store/app.state';
import { CustomersHelper } from '../../helpers/customers.helper';

import { CustomersCreateEditComponent } from './customers-create-edit.component';

describe('CustomersCreateEditComponent', () => {
  let component: CustomersCreateEditComponent;
  let fixture: ComponentFixture<CustomersCreateEditComponent>;
  let store: Store<AppState>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule],
      declarations: [ CustomersCreateEditComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [CustomerService, CustomersHelper, { provide: Store, useValue: store }]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomersCreateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    store = TestBed.inject(Store);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

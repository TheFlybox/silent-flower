import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { nanoid } from 'nanoid';
import { MenuItem } from 'primeng/api';
import { CustomersRoutes } from 'src/app/core/enums/app-routes.enum';
import { CustomerStatus } from 'src/app/core/enums/customers-status.enum';
import { Customer, CustomerForm } from 'src/app/core/models/customer.model';
import { DropdownOption } from 'src/app/core/models/dropdown-option.model';
import { AppMessagesService } from 'src/app/core/services/app-messages.service';
import { CustomerService } from 'src/app/core/services/customer.service';
import { NavigationService } from 'src/app/core/services/navigation.service';
import { CustomersHelper } from '../../helpers/customers.helper';

@Component({
  templateUrl: './customers-create-edit.component.html',
  styleUrls: ['./customers-create-edit.component.scss']
})
export class CustomersCreateEditComponent implements OnInit {
  public breadcrumbItems: MenuItem[] = [];
  public isEditing: boolean = false;
  public customerForm?: CustomerForm;
  public customerStatusList: DropdownOption[] = [];
  public header: string = '';
  public saveButtonClicked: boolean = false;
  public customer: Partial<Customer> = {
    firstName: '',
    lastName: '',
    email: '',
    status: CustomerStatus.ACTIVE,
    phone: ''
  };

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private customerService: CustomerService,
    private customersHelper: CustomersHelper,
    private navigationService: NavigationService,
    private appMessageService: AppMessagesService,
  ) {}

  ngOnInit(): void {
    this.setMode();
    this.customerForm = this.buildForm();
    this.customerStatusList = this.buildStatusList();
    this.setBreadCrumbItems();
    this.setHeading();
  }

  public goBack() {
    this.navigationService.back();
  }

  public onResetClick(): void {
    if(this.customerForm) {
      this.customerForm.reset();
    }
  }

  public onSaveClick() {
    this.saveButtonClicked = true;
    if(this.customerForm?.invalid) return;
    const data = this.customerForm?.value;
    let customer: Customer | null = null;
    if(this.isEditing) {
      customer = this.customersHelper.convertFormDataToCustomer(this.customer.id!, data!);
      this.customerService.updateCustomer({...customer, identity: this.customer.identity});
      this.appMessageService.addMessage({
        id: nanoid(),
        action: 'EDIT',
        isRead: false,
        message: {
          severity: 'success',
          detail: `Customer ${customer.id} updated successfully!`,
          summary: 'Customer updated'
        }
      });
    } else {
      const newId = nanoid();
      customer = this.customersHelper.convertFormDataToCustomer(newId, {...data, });
      this.customerService.createCustomer(customer);
      this.appMessageService.addMessage({
        id: nanoid(),
        action: 'CREATE',
        isRead: false,
        message: {
          severity: 'success',
          detail: `Customer added successfully!`,
          summary: 'Customer added!'
        }
      });
    }
    this.customersHelper.goToCustomersList();
  }

  private setMode() {
    const routeData = this.activatedRoute.snapshot.data['customer'];
    if(routeData) {
      this.customer = routeData;
      this.isEditing = true;
    } else {
      this.isEditing = false;
    }
  }

  private setHeading() {
    if(this.isEditing) this.header = 'Edit customer';
    else this.header = 'New customer';
  }

  private setBreadCrumbItems() {
    this.breadcrumbItems = [{ label: 'Customers', routerLink: `../${CustomersRoutes.LIST }`}];
    if(!this.isEditing) {
      this.breadcrumbItems.push({ label: 'Create' })
    }else {
      this.breadcrumbItems.push({ label: 'Edit' }, { label: this.customer.id });
    }
  }

  private buildForm(): CustomerForm {
    return this.formBuilder.nonNullable.group({
      firstName: this.formBuilder.nonNullable.control(
        this.customer.firstName ?? '',
        [Validators.required]
      ),
      lastName: this.formBuilder.nonNullable.control(
        this.customer.lastName ?? '',
        [Validators.required]
      ),
      status: this.formBuilder.nonNullable.control(this.customer.status!, [
        Validators.required,
      ]),
      email: this.formBuilder.nonNullable.control(this.customer.email ?? '', [
        Validators.required,
        Validators.email,
      ]),
      phone: this.formBuilder.nonNullable.control(this.customer.phone ?? ''),
    });
  }

  private buildStatusList(): DropdownOption[] {
    return Object.keys(CustomerStatus).map(x => {
      const key = x as keyof typeof CustomerStatus;
      return {
        label: CustomerStatus[key],
        value: CustomerStatus[key]
      }
    });
  }

}

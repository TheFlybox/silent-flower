import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Customer } from 'src/app/core/models/customer.model';
import { NavigationService } from 'src/app/core/services/navigation.service';
import { CustomersHelper } from '../../helpers/customers.helper';

@Component({
  templateUrl: './customers-detail.component.html',
  styleUrls: ['./customers-detail.component.scss']
})
export class CustomersDetailComponent implements OnInit {
  public customer?: Customer;

  constructor(
    private activatedRoute: ActivatedRoute,
    private navigationService: NavigationService,
    private customersHelper: CustomersHelper,
  ) {}

  ngOnInit(): void {
    this.customer = this.activatedRoute.snapshot.data['customer'];
    if(!this.customer) this.goToCustomersList();
  }

  public goBack() {
    this.navigationService.back();
  }

  public goToCustomersList() {
    this.customersHelper.goToCustomersList();
  }
}

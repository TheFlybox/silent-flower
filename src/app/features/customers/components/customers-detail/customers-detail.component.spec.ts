import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NavigationService } from 'src/app/core/services/navigation.service';
import { CustomersHelper } from '../../helpers/customers.helper';

import { CustomersDetailComponent } from './customers-detail.component';

describe('CustomersDetailComponent', () => {
  let component: CustomersDetailComponent;
  let fixture: ComponentFixture<CustomersDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ CustomersDetailComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [CustomersHelper, NavigationService]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomersDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

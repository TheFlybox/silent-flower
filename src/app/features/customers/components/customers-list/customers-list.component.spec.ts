import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { AppMessagesService } from 'src/app/core/services/app-messages.service';
import { CustomerService } from 'src/app/core/services/customer.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { AppState } from 'src/app/store/app.state';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { CustomersListComponent } from './customers-list.component';
import { Dictionary } from '@ngrx/entity';
import { Customer } from 'src/app/core/models/customer.model';
import { nanoid } from 'nanoid';

describe('CustomersListComponent', () => {
  let component: CustomersListComponent;
  let fixture: ComponentFixture<CustomersListComponent>;
  let store: Store<AppState>;
  let initialState: AppState = {
    customers: {
      entities: {
        '1': {
          id: nanoid(),
          identity: 1,
          firstName: 'Test',
          lastName: 'Testing',
          email: 'test@test.com',
          phone: '123-111-1111'
        } as Customer
      } as Dictionary<Customer>,
      ids: ['1'],
      totalRecords: 1,
    },
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule],
      declarations: [CustomersListComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: Store, useValue: store },
        ConfirmationService,
        MessageService,
        AppMessagesService,
        CustomerService,
        provideMockStore({initialState})
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CustomersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    store = TestBed.inject(Store);
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should use paginator and list 1 elements', () => {
    fixture.detectChanges();
    let table: Table = component.customersTable!;

    table.paginator = true;
    table.rows = 1;
    fixture.detectChanges();

    const tableEl = fixture.debugElement.query(By.css('div'));
    const bodyRows = tableEl.query(By.css('.p-datatable-tbody')).queryAll(By.css('tr'));
    expect(bodyRows.length).toEqual(1);
    fixture.detectChanges();

    expect(bodyRows.length).toEqual(1);
  });
});

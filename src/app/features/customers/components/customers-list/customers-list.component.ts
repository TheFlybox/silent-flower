import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { nanoid } from 'nanoid';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { Subscription } from 'rxjs';
import { CustomersRoutes } from 'src/app/core/enums/app-routes.enum';
import { CustomerStatus } from 'src/app/core/enums/customers-status.enum';
import { TableOperators } from 'src/app/core/enums/table-operators.enum';
import { CustomersActionsInterface } from 'src/app/core/interfaces/customers-actions.interface';
import { Customer } from 'src/app/core/models/customer.model';
import { DropdownOption } from 'src/app/core/models/dropdown-option.model';
import { GridColumn } from 'src/app/core/models/grid-column.model';
import { AppMessagesService } from 'src/app/core/services/app-messages.service';
import { CustomerService } from 'src/app/core/services/customer.service';
import { CUSTOMERS_ACTIONS_MENU } from '../../data/customer-actions-menu.data';
import { CUSTOMER_SEARCH_OPTIONS } from '../../data/customer-search-options.data';
import { CUSTOMER_STATUS_SEARCH } from '../../data/customer-status-search.data';
import { CUSTOMERS_GRID_COLUMNS } from '../../data/customers-columns.data';
import { CUSTOMERS_GRID_SETTINGS } from '../../data/customers-grid-settings.data';

@Component({
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.scss']
})
export class CustomersListComponent implements CustomersActionsInterface, OnInit, OnDestroy {
  @ViewChild('customersTable') customersTable?: Table;

  public customerList: Customer[] = [];
  public customerColumns: GridColumn<Customer>[] = CUSTOMERS_GRID_COLUMNS;
  public customerGridSettings = CUSTOMERS_GRID_SETTINGS;
  public customerSearchOptions = CUSTOMER_SEARCH_OPTIONS;
  public customerStatusSearch = CUSTOMER_STATUS_SEARCH;
  public selectedCustomerStatusSearch: DropdownOption = this.customerStatusSearch[0];
  public selectedCustomers: Customer[] = [];
  public actionsMenu: MenuItem[] = CUSTOMERS_ACTIONS_MENU(this);
  private subscriptions = new Subscription();

  get total() { return this.customerList.length }
  get activeCustomers() { return this.customerList.filter(x => x.status === CustomerStatus.ACTIVE).length }
  get inactiveCustomers() { return this.customerList.filter(x => x.status === CustomerStatus.INACTIVE).length }
  get pendingCustomers() { return this.customerList.filter(x => x.status === CustomerStatus.PENDING).length }

  get POPUP_KEY() { return 'popup' }
  get DIALOG_KEY() { return 'dialog' }
  get TOAST_KEY() { return 'toast' }

  constructor(
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private customerService: CustomerService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private appMessageService: AppMessagesService,
  ) {}

  ngOnInit(): void {
    this.setMessagesBehavior();
    this.subscriptions.add(
      this.customerService.getAllCustomers().subscribe(data => {
        this.customerList = data.records.sort((a, b)=> b.identity! - a.identity!);
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  private setMessagesBehavior() {
    this.subscriptions.add(
      this.appMessageService.messagesObservable.subscribe(messages => {
        setTimeout(() => {
          messages.forEach(message => {
            if(!message.isRead) {
              this.messageService.add({key: this.TOAST_KEY, ...message.message});
              this.appMessageService.setMessageReaded(message.id!);
            }
          })
        }, 100);
      })
    );
  }

  public onCustomerStatusSearch(event: DropdownOption) {
    if(!this.customersTable) return;
    if(event.label === 'All') this.customersTable.clear();
    else this.customersTable.filter(event.label, event.value, TableOperators.EQUALS);
  }

  public onSearchInput(event: { field: string, value: string }) {
    if(this.customersTable) {
      this.customersTable.filter(event.value, event.field, TableOperators.CONTAINS);
    }
  }

  public onToolbarDeleteClick(event: MouseEvent) {
    this.confirmationService.confirm({
      key: this.POPUP_KEY,
      target: event.target!,
      message: 'Are you sure that you want to delete the selected customers?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        new Promise(resolve => {
          this.selectedCustomers.forEach(x => this.customerService.removeCustomer(x.id));
          this.selectedCustomers = [];
          this.appMessageService.addMessage({
            id: nanoid(),
            action: 'DELETE',
            isRead: false,
            message: {
              severity: 'info',
              detail: 'Bulk remove successfully completed!',
              summary: 'Remove customers'
            }
          });
          resolve(true);
        });
      },
    });
  }

  openDetailView(customerId: string): void {
    const url = CustomersRoutes.DETAIL.toString().split('/');
    this.router.navigate([`../${url[0]}`, customerId], { relativeTo: this.activatedRouter });
  }

  openCreateView(): void {
    this.router.navigate([`../${CustomersRoutes.CREATE}`], { relativeTo: this.activatedRouter });
  }

  openEditView(customerId: string): void {
    const url = CustomersRoutes.EDIT.toString().split('/');
    this.router.navigate([`../${url[0]}`, customerId], { relativeTo: this.activatedRouter });
  }

  removeCustomer(customerId: string): void {
    this.confirmationService.confirm({
      key: this.DIALOG_KEY,
      message: 'Are you sure that you want to delete this customers?',
      accept: () => {
        this.customerService.removeCustomer(customerId);
        this.appMessageService.addMessage({
          id: nanoid(),
          action: 'DELETE',
          isRead: false,
          message: {
            severity: 'success',
            detail: `Customer ${customerId} removed successfully!`,
            summary: 'Remove customer'
          }
        });
      },
    });
  }
}

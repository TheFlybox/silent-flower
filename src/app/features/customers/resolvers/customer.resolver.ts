import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";
import { AppRoutes, CustomersRoutes } from "src/app/core/enums/app-routes.enum";
import { Customer } from "src/app/core/models/customer.model";
import { CustomerService } from "src/app/core/services/customer.service";

@Injectable()
export class CustomerResolver implements Resolve<Observable<Customer>> {
  constructor(
    private customerService: CustomerService,
    private router: Router,
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Customer> | Observable<Observable<Customer>> | Promise<Observable<Customer>> {
    const customerId: string = route.paramMap.get('id') ?? '';
    return this.customerService.getCustomerById(customerId);
  }
}
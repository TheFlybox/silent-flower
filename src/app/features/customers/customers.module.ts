import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersComponent } from './customers.component';
import { CustomersListComponent } from './components/customers-list/customers-list.component';
import { CustomersCreateEditComponent } from './components/customers-create-edit/customers-create-edit.component';
import { CustomersDetailComponent } from './components/customers-detail/customers-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CustomerResolver } from './resolvers/customer.resolver';
import { CustomersHelper } from './helpers/customers.helper';
import { ConfirmationService, MessageService } from 'primeng/api';
import { CustomerActionMenuPipe } from './pipes/customer-action-menu.pipe';


@NgModule({
  declarations: [
    CustomersComponent,
    CustomersListComponent,
    CustomersCreateEditComponent,
    CustomersDetailComponent,
    CustomerActionMenuPipe
  ],
  imports: [
    CommonModule,
    CustomersRoutingModule,
    SharedModule
  ],
  providers: [CustomerResolver, CustomersHelper, MessageService, ConfirmationService]
})
export class CustomersModule { }

import { nanoid } from "nanoid";
import { CustomerStatus } from "src/app/core/enums/customers-status.enum";
import { DropdownOption } from "src/app/core/models/dropdown-option.model";

export const CUSTOMER_STATUS_SEARCH: DropdownOption[] = [
  {
    id: nanoid(),
    label: 'All',
    value: 'status'
  },
  {
    id: nanoid(),
    label: CustomerStatus.ACTIVE,
    value: 'status'
  },
  {
    id: nanoid(),
    label: CustomerStatus.INACTIVE,
    value: 'status'
  },
  {
    id: nanoid(),
    label: CustomerStatus.PENDING,
    value: 'status'
  }
];
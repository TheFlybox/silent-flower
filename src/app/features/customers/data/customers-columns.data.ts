import { nanoid } from "nanoid";
import { Customer } from "src/app/core/models/customer.model";
import { GridColumn } from "src/app/core/models/grid-column.model";

export const CUSTOMERS_GRID_COLUMNS: GridColumn<Customer>[] = [
  {
    id: nanoid(),
    header: 'First Name',
    fieldName: 'firstName',
    isSortable: true
  },
  {
    id: nanoid(),
    header: 'Last Name',
    fieldName: 'lastName',
    isSortable: true
  },
  {
    id: nanoid(),
    header: 'Status',
    fieldName: 'status',
    isSortable: true
  },
  {
    id: nanoid(),
    header: 'Email',
    fieldName: 'email'
  },
  {
    id: nanoid(),
    header: 'Phone',
    fieldName: 'phone'
  }
];
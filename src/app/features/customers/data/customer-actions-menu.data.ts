import { nanoid } from "nanoid"
import { MenuItem } from "primeng/api"
import { CustomersActionsInterface } from "src/app/core/interfaces/customers-actions.interface"
import { Customer } from "src/app/core/models/customer.model"

export const CUSTOMERS_ACTIONS_MENU = (component: CustomersActionsInterface): MenuItem[] => {
  return [
    {
      id: nanoid(),
      label: 'View',
      command: (event: { originalEvent: MouseEvent, item: MenuItem & { data: Customer } }) => {
        component.openDetailView(event.item.data.id);
      }
    },
    {
      id: nanoid(),
      label: 'Edit',
      command: (event: { originalEvent: MouseEvent, item: MenuItem & { data: Customer } }) => {
        component.openEditView(event.item.data.id)
      }
    },
    {
      id: nanoid(),
      label: 'Delete',
      command: (event: { originalEvent: MouseEvent, item: MenuItem & { data: Customer } }) => {
        component.removeCustomer(event.item.data.id)
      }
    },
  ]
}
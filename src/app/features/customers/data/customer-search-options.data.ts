import { nanoid } from "nanoid";
import { DropdownOption } from "src/app/core/models/dropdown-option.model";

export const CUSTOMER_SEARCH_OPTIONS: DropdownOption[] = [
  {
    id: nanoid(),
    label: 'Last Name',
    value: 'lastName',
    placeholder: 'Search by Last Name'
  },
  {
    id: nanoid(),
    label: 'First Name',
    value: 'firstName',
    placeholder: 'Search by First Name'
  },
  {
    id: nanoid(),
    label: 'Email',
    value: 'email',
    placeholder: 'Search by Email'
  },
  {
    id: nanoid(),
    label: 'Phone',
    value: 'phone',
    placeholder: 'Search by Phone'
  }
];

export const CUSTOMERS_GRID_SETTINGS = {
  paginator: true,
  rows: 10,
  showCurrentPageReport: true,
  responsiveLayout: 'stack',
  currentPageReportTemplate: 'Showing {first} to {last} of {totalRecords} entries',
  rowsPerPageOptions: [10, 25, 50],
  rowHover: true,
  dataKey: 'id',
  scrollHeight: 'flex',
  sortMode: 'multiple'
}
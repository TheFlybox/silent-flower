import { Pipe, PipeTransform } from "@angular/core";
import { MenuItem } from "primeng/api";
import { Customer } from "src/app/core/models/customer.model";

@Pipe({name: 'customerActionMenu'})
export class CustomerActionMenuPipe implements PipeTransform{
  transform(value: MenuItem[], customer: Customer, ...args: any[]) {
    return value.map(x => ({...x, data: customer}));
  }
}
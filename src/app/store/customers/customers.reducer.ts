import { Update } from "@ngrx/entity";
import { createReducer, on } from "@ngrx/store";
import { Customer } from "src/app/core/models/customer.model";
import { createCustomer, listCustomers, removeCustomer, updateCustomer } from "./customers.actions";
import { customersAdapter, customersInitialState } from "./customers.state";

export const customersReducer = createReducer(
  customersInitialState,
  on(
    listCustomers,
    (
      state,
      { payload }: { payload: { customers: Customer[]; totalRecords: number } }
    ) => {
      return customersAdapter.setAll(payload.customers, {
        ...state,
        totalRecords: payload.totalRecords,
      });
    }
  ),
  on(createCustomer, (state, { customer }: { customer: Customer }) => {
    const index = state.totalRecords + 1;
    return customersAdapter.addOne({...customer, identity: index}, state);
  }),
  on(updateCustomer, (state, { payload }: { payload: Update<Customer> }) => {
    return customersAdapter.updateOne(payload, state);
  }),
  on(removeCustomer, (state, { customerId }: { customerId: string }) => {
    return customersAdapter.removeOne(customerId, state);
  })
);
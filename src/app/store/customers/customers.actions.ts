import { Update } from "@ngrx/entity";
import { createAction, props } from "@ngrx/store";
import { Customer } from "src/app/core/models/customer.model";

export enum CustomersActionTypes {
  CREATE = '[Customer] Create a customer',
  UPDATE = '[Customer] Update a customer',
  REMOVE = '[Customer] Remove a customer',
  LIST = '[Customer] List of customers'
}

export const listCustomers = createAction(
  CustomersActionTypes.LIST,
  props<{payload: { customers: Customer[], totalRecords: number }}>()
);

export const createCustomer = createAction(
  CustomersActionTypes.CREATE,
  props<{customer: Customer}>()
);

export const updateCustomer = createAction(
  CustomersActionTypes.UPDATE,
  props<{payload: Update<Customer>}>()
);

export const removeCustomer = createAction(
  CustomersActionTypes.REMOVE,
  props<{customerId: string}>()
);
import { createEntityAdapter, EntityAdapter, EntityState } from "@ngrx/entity";
import { Customer } from "src/app/core/models/customer.model";

export interface CustomerState extends EntityState<Customer> {
  totalRecords: number;
}

export const customersAdapter: EntityAdapter<Customer> = createEntityAdapter<Customer>({
  sortComparer: (c1: Customer, c2: Customer) => c1.id.localeCompare(c2.id),
  selectId: (customer: Customer) => customer.id
});

export const customersInitialState = customersAdapter.getInitialState<CustomerState>({
  ids: [],
  entities: null as any,
  totalRecords: 0
});
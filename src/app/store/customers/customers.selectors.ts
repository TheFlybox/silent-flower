import { createSelector } from "@ngrx/store";
import { AppUtils } from "src/app/core/utils/app-utils";
import { AppState } from "../app.state";

const selectCustomers = (state: AppState) => state.customers;

export const customers = createSelector(selectCustomers, (state) => ({
  records: AppUtils.convertDictionaryToArray(state.entities),
  totalRecords: state.totalRecords,
}));

export const customerById = (customerId: string) => createSelector(selectCustomers, (state) => ({
  record: AppUtils.convertDictionaryToArray(state.entities).filter(x => x.id === customerId)[0]
}));
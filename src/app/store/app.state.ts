import { customersReducer } from "./customers/customers.reducer"
import { CustomerState } from "./customers/customers.state"

export interface AppState {
  customers: CustomerState
}

export const appReducer = {
  customers: customersReducer,
}
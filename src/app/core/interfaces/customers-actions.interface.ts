
export interface CustomersActionsInterface {
  openDetailView(customerId: string): void;
  openCreateView(): void;
  openEditView(customerId: string): void;
  removeCustomer(customerId: string): void;
}
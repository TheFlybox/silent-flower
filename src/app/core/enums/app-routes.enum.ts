
export enum AppRoutes {
  CUSTOMERS = 'customers'
}

export enum CustomersRoutes {
  LIST = 'list',
  DETAIL = 'detail/:id',
  CREATE = 'create',
  EDIT = 'edit/:id'
}
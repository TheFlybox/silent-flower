
export enum AppTokens {
  LOCAL_STORAGE = '[Storage] Local Storage',
  WINDOW = '[Global] Window Object'
}
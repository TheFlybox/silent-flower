
export interface GridColumn<T> {
  id: string;
  header: string;
  fieldName: keyof T;
  isSortable?: boolean;
}
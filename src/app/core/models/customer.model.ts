import { FormControl, FormGroup } from "@angular/forms";
import { Message } from "primeng/api";
import { CustomerStatus } from "../enums/customers-status.enum";

export interface Customer {
  id: string;
  firstName: string;
  lastName: string;
  status: CustomerStatus;
  email: string;
  phone: string;
  identity?: number;
}

export interface CustomerForm extends FormGroup<{
  [key in keyof Omit<Customer, 'id'>]: FormControl<Customer[key]>
}> {}

export interface AppMessage {
  id?: string;
  action: 'CREATE' | 'EDIT' | 'DELETE',
  message: Message;
  isRead: boolean;
}

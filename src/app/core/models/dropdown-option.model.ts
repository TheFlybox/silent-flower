
export interface DropdownOption {
  id?: string;
  label: string; 
  value: string;
  placeholder?: string;
}
import { Dictionary } from "@ngrx/entity";

export class AppUtils {
  static convertDictionaryToArray<T>(dictionary: Dictionary<T>) {
    if(!dictionary) return [];
    let result: T[] = [];
    Object.keys(dictionary).forEach(key => result.push(dictionary[key]!));
    return result;
  }
}
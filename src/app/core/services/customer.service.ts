import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { map } from "rxjs";
import { AppState } from "src/app/store/app.state";
import {
  createCustomer,
  listCustomers,
  removeCustomer,
  updateCustomer,
} from 'src/app/store/customers/customers.actions';
import { customerById, customers } from "src/app/store/customers/customers.selectors";
import { Customer } from "../models/customer.model";

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  constructor(private store: Store<AppState>) {}

  public loadCustomers(customers: Customer[] = []) {
    this.store.dispatch(listCustomers({payload: { customers, totalRecords: customers.length }}));
  }

  public getAllCustomers() {
    return this.store.select(customers);
  }

  public getCustomerById(customerId: string) {
    return this.store.select(customerById(customerId)).pipe(map(data => data.record));
  }

  public createCustomer(customer: Customer) {
    this.store.dispatch(createCustomer({customer}));
  }

  public updateCustomer(customer: Customer) {
    this.store.dispatch(updateCustomer({payload: { id: customer.id, changes: customer }}));
  }

  public removeCustomer(customerId: string) {
    this.store.dispatch(removeCustomer({customerId}));
  }
}
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { AppMessage } from "../models/customer.model";

@Injectable({
  providedIn: 'root'
})
export class AppMessagesService {
  private _messages = new BehaviorSubject<AppMessage[]>([]);
  public messagesObservable = this._messages.asObservable();

  public addMessage(message: AppMessage) {
    const temp = this._messages.value;
    temp.push(message);
    this._messages.next(temp);
  }

  public setMessageReaded(id: string) {
    const temp = this._messages.value;
    const index = temp.findIndex(x => x.id ===id);
    temp[index].isRead = true;
    this._messages.next(temp);
  }
}
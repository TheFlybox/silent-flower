import { Injectable, OnDestroy } from "@angular/core";
import { Subscription, take } from "rxjs";
import { StorageKeys } from "../enums/storage-keys.enum";
import { CustomerService } from "./customer.service";
import { LocalStorageService } from "./local-storage.service";
import { MockDataService } from "./mock-data.service";

@Injectable({
  providedIn: 'root'
})
export class AppInitService implements OnDestroy{
  private subscription?: Subscription;

  constructor(
    private customerService: CustomerService,
    private localStorageService: LocalStorageService,
    private mockDataService: MockDataService
  ) {}
  
  ngOnDestroy(): void {
    if(this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  public async initialize(): Promise<void> {
    this.loadCustomers();
  }

  private loadCustomers() {
    this.subscription = this.localStorageService.pipe(take(1)).subscribe(data => {
      if(data.has(StorageKeys.CUSTOMERS)) {
        const customers = JSON.parse(data.get(StorageKeys.CUSTOMERS)!);
        this.customerService.loadCustomers(customers);
      }else {
        const mockSize: number = 20;
        this.mockDataService.getListOfCustomers(mockSize).then(customers => {
          this.customerService.loadCustomers(customers);
          this.localStorageService.addToStorage(StorageKeys.CUSTOMERS, JSON.stringify(customers));
        });
      }
    })
  }
}
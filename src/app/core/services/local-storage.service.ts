import { Inject, Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LOCAL_STORAGE } from '../tokens/local-storage.token';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService
  extends Observable<Map<string, string>>
  implements OnDestroy
{
  private readonly _entireStorage$: BehaviorSubject<Map<string, string>> =
    new BehaviorSubject(this._allStorage());

  constructor(@Inject(LOCAL_STORAGE) private _localStorage: Storage) {
    super((subscriber) => {
      this._entireStorage$.subscribe(subscriber);
    });
  }

  ngOnDestroy(): void {
    this._entireStorage$.complete();
  }

  public addToStorage(key: string, value: string) {
    this._localStorage.setItem(key, value);
    this._entireStorage$.next(this._allStorage());
  }

  public removeFromStorage(key: string) {
    key && this._localStorage.removeItem(key);
    this._entireStorage$.next(this._allStorage());
  }

  private _allStorage(): Map<string, string> {
    let values = new Map(),
      keys = Object.keys(this._localStorage),
      i = keys.length;
    while (i--) {
      values.set(keys[i], this._localStorage.getItem(keys[i]));
    }
    return values;
  }
}

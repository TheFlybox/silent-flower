import { Injectable } from "@angular/core";
import { nanoid } from "nanoid";
import { Customer } from "../models/customer.model";
import { faker } from '@faker-js/faker';
import { CustomerStatus } from "../enums/customers-status.enum";

@Injectable({
  providedIn: 'root'
})
export class MockDataService {  
  public async getListOfCustomers(size: number): Promise<Customer[]> {
    const data: Customer[] = [];
    Array.from({length: size}).forEach((x, i) => data.push(this.createRandomCustomer(i)));
    return data;
  }

  private createRandomCustomer(index: number): Customer {
    return {
      id: nanoid(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      status: faker.helpers.arrayElement([
        CustomerStatus.ACTIVE,
        CustomerStatus.INACTIVE,
        CustomerStatus.PENDING,
      ]),
      phone: faker.phone.number('###-###-####'),
      identity: index
    };
  }
}
import { InjectionToken, inject } from "@angular/core";
import { AppTokens } from '../enums/app-tokens.enum';
import { WINDOW } from './window.token';

export const LOCAL_STORAGE = new InjectionToken<Storage>(AppTokens.LOCAL_STORAGE, {
  factory: () => inject(WINDOW).localStorage,
});
import { DOCUMENT } from '@angular/common';
import { inject, InjectionToken } from '@angular/core';
import { AppTokens } from '../enums/app-tokens.enum';

export const WINDOW = new InjectionToken<Window>(AppTokens.WINDOW, {
  factory: () => {
    const { defaultView } = inject(DOCUMENT);

    if (!defaultView) {
      throw new Error('Window is not available!');
    }

    return defaultView;
  },
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersBadgeComponent } from './customers-badge.component';

describe('CustomersBadgeComponent', () => {
  let component: CustomersBadgeComponent;
  let fixture: ComponentFixture<CustomersBadgeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomersBadgeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomersBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { CustomerStatus } from 'src/app/core/enums/customers-status.enum';

@Component({
  selector: 'app-customers-badge',
  templateUrl: './customers-badge.component.html',
  styleUrls: ['./customers-badge.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomersBadgeComponent {
  @Input() status: CustomerStatus = CustomerStatus.ACTIVE;

  get statusClass() {
    let status = this.status ?? CustomerStatus.ACTIVE;
    return `status-${status.toLowerCase()}`;
  } 
}

import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-simple-stat',
  templateUrl: './simple-stat.component.html',
  styleUrls: ['./simple-stat.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleStatComponent implements OnInit {
  @Input() label: string = '';
  @Input() count: number = 0;
  @Input() color: 'red' | 'cyan' | 'green' | 'yellow' = 'cyan';
  @Input() icon: string = '';
  @Input() margin: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

}

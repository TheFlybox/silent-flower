import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NavigationService } from 'src/app/core/services/navigation.service';

@Component({
  selector: 'app-back-button',
  templateUrl: './back-button.component.html',
  styleUrls: ['./back-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BackButtonComponent {
  constructor(private navigationService: NavigationService) {}
  public goBack(): void {
    this.navigationService.back();
  }
}

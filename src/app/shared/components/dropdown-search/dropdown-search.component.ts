import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { debounceTime, distinctUntilChanged, filter, fromEvent, Subscription, tap } from 'rxjs';
import { DropdownOption } from 'src/app/core/models/dropdown-option.model';

@Component({
  selector: 'app-dropdown-search',
  templateUrl: './dropdown-search.component.html',
  styleUrls: ['./dropdown-search.component.scss'],
})
export class DropdownSearchComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('searchInput') searchInput?: ElementRef;
  @Input() options: DropdownOption[] = [];
  @Input() size?: 'small' | 'normal' | 'large';
  @Output() onSearchEvent = new EventEmitter<{
    field: string;
    value: string;
  }>();

  public searchSubscription?: Subscription;
  public inputValue: string = '';
  public selectedOption?: DropdownOption;
  public get sizeClass() {
    if (!this.size) return '';
    switch (this.size) {
      case 'small':
        return 'p-inputtext-sm';
      case 'normal':
        return '';
      case 'large':
        return 'p-inputtext-lg';
    }
  }

  ngOnInit() {
    if(this.options.length > 0) {
      this.selectedOption = this.options[0];
    }
  }

  ngAfterViewInit(): void {
    if(this.searchInput) {
      const time = 750;
      this.searchSubscription = fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(
        filter(Boolean),
        debounceTime(time),
        distinctUntilChanged(),
        tap(() => this.onSearch())
      ).subscribe();
    }
  }

  ngOnDestroy(): void {
    this.searchSubscription?.unsubscribe();
  }

  resetInputValue() {
    this.inputValue = '';
  }

  onSearch() {
    this.onSearchEvent.emit({
      field: this.selectedOption?.value!,
      value: this.inputValue,
    });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MenubarModule } from 'primeng/menubar';
import { MenuModule } from 'primeng/menu';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputMaskModule } from 'primeng/inputmask';
import { ToolbarModule } from 'primeng/toolbar';
import { DropdownModule } from 'primeng/dropdown';
import { SelectButtonModule } from 'primeng/selectbutton';
import { AccordionModule } from 'primeng/accordion';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ToastModule } from 'primeng/toast';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

import { DropdownSearchComponent } from './components/dropdown-search/dropdown-search.component';
import { CustomersBadgeComponent } from './components/customers-badge/customers-badge.component';
import { SimpleStatComponent } from './components/simple-stat/simple-stat.component';
import { BackButtonComponent } from './components/back-button/back-button.component';
import { ClickStopPropagation } from './directives/click-stop-propagation.directive';

@NgModule({
  declarations: [
    DropdownSearchComponent,
    CustomersBadgeComponent,
    SimpleStatComponent,
    BackButtonComponent,
    ClickStopPropagation
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MenubarModule,
    ButtonModule,
    InputTextModule,
    TableModule,
    RadioButtonModule,
    InputMaskModule,
    ToolbarModule,
    DropdownModule,
    SelectButtonModule,
    AccordionModule,
    BreadcrumbModule,
    ToastModule,
    MenuModule,
    OverlayPanelModule,
    ConfirmPopupModule,
    ConfirmDialogModule,
  ],
  exports: [
    MenubarModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    InputTextModule,
    TableModule,
    RadioButtonModule,
    InputMaskModule,
    ToolbarModule,
    DropdownModule,
    DropdownSearchComponent,
    SelectButtonModule,
    CustomersBadgeComponent,
    AccordionModule,
    SimpleStatComponent,
    BreadcrumbModule,
    BackButtonComponent,
    ToastModule,
    MenuModule,
    OverlayPanelModule,
    ClickStopPropagation,
    ConfirmPopupModule,
    ConfirmDialogModule,
  ],
})
export class SharedModule {}
